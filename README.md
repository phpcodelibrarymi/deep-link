# README #

### Added two demo of deep linking. ###
* **Laravel:** In Laravel folder you can find the deep link coding structure.
* **Core PHP:** In core_php_demo_file.php file you can find code for deep link
                   
                   
#### Required things from iOS
- App Schema
- iTunes Store URL

#### Required things from Android
- App Schema
- Google Play Store URL

#### Code to Change for iOS
```javascript 
var appstoreFail = “https://www.apple.com/in/itunes/”; 
```
##### Description::: Here you have to replace the url with iTunes store URL of the app. It will redirect user to iTunes store if app is not downloaded.

```javascript 
var appUrlSchemeIOS = "project_name://video/<?php echo $data['id']; ?>";
```
##### Description::: This is for redirecting user after they click on deep link.
- **project_name** :: Here you can write your project name
- **video** :: Here you can give sub level structure e.g. product, reset_password etc.
-  **echo $data['id']** ::: Here you will have to pass your dynamic id for deep linking

#### Code to Change for Android
```javascript 
var appUrlSchemeAndroid = 'project_name/video/<?php echo $data['id']; ?>';
```
##### Description::: this is for redirecting user after they click on deep link.
- **project_name** :: Here you can write your project name
- **video** :: Here you can give sub level structure e.g. product, reset_password etc.
-  **echo $data['id']** ::: Here you will have to pass your dynamic id for deep linking

```javascript 
document.location = "intent://"+appUrlSchemeAndroid+"#Intent;scheme=https;package=com.project_name;end";
```
##### Description ::: Here you have to replace the package name(com.project_name) with android app package name.

```javascript 
window.location = "market://details?id=https://play.google.com/store/apps/details?id=com.project_name”;
```
##### Description::: Here you have to replace the url(https://play.google.com/store/apps/details?id=com.project_name) with Google Play store URL of the app. It will redirect user to play store if app is not downloaded.