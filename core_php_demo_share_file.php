<?php
/* JUST DEMO DATA. SET YOUR DYNAMIC DATA */
$shareData = new \stdClass();
$shareData->id = 1;
$shareData->title = "Share Title";
$shareData->description = "Share Details";
$shareData->image = "http://envoc.com/uploads/images/think-professional.jpg";
$shareData->video_url = "https://www.youtube.com/";
$data = array();
$data['id'] = $shareData->id;
$data['title'] = $shareData->title;
$data['description'] = $shareData->description;
$data['image'] = $shareData->image;
$data['video_url'] = $shareData->video_url;
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:url"
          content="<?php echo $_SERVER['REQUEST_URI']; ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="<?php echo $data['title']; ?> "/>
    <meta property="og:description" content=" <?php echo $data['description']; ?>"/>
    <meta property="og:image" content="<?php echo $data['image']; ?>"/>
    <meta property='og:video' content='<?php echo $data['video_url']; ?>' />
    <title>Demo share video</title>
    <link href="https://fonts.googlea
pis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            display: block;
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>

    <script>
        var appstoreFail = "https://www.apple.com/in/itunes/"; // replace URL when APP will be live on itunes. Here i have Eg:: var appstoreFail = "https://itunes.apple.com/us/app/wishsprout/id1243493154?ls=1&mt=8";
        //Check is device is iOS
        var iOS = (navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
        var web_browser = (navigator.userAgent.match(/(Firefox|Chrome|Safari)/g) ? true : false );
        var ua = navigator.userAgent.toLowerCase();
        var isAndroid = ua.indexOf("android") > -1;
        var appUrlSchemeAndroid = 'project_name/video/<?php echo $data['id']; ?>';
        var appUrlSchemeIOS = "project_name://<?php echo $data['id']; ?>";
        //alert(isAndroid);
        if (iOS) {
            //If the app ais not installed the script will wait for 2sec and redirect to web.

            var loadedAt = +new Date;
            setTimeout(
                    function () {
                        if (+new Date - loadedAt < 2000) {
                            window.location = appstoreFail;
                        }
                    }
                    , 25);
            //Try launching the app using URL schemes
            window.open(appUrlSchemeIOS, "_self");
        }

        else if(isAndroid){
            var checkAndLaunch = function() {
                var fallbackFunction = function() {
                    window.location = "market://details?id=https://play.google.com/store/apps/details?id=com.project_name";
                };
                var loadChromeIntent = function() {
                    method = 'intent';
                   // alert("yes"+appUrlSchemeAndroid);
                    document.location = "intent://"+appUrlSchemeAndroid+"#Intent;scheme=https;package=com.project_name;end";
                };
                loadChromeIntent();
                setTimeout(fallbackFunction, 750);
            };
            window.onload=checkAndLaunch;
        }

    </script>
</head>


<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <p>You are sharing the following file on social media: <?php echo $data['title'];?></p>
        <p>Download Links to PROJECT_NAME Apps</p>
        <p>
            <a href="project_name://<?php echo $data['id']; ?>">Test-Link to file in app iOS</a>
        </p>
        <p>
            <a href="">Test-Link to file in app Android</a>
        </p>
    </div>
</div>
</body>
</html>
