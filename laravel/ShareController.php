<?php

namespace App\Http\Controllers;

use App\Api\ApiController;
use App\Api\V1\Controllers\ActivityController;
use App\Api\V1\Controllers\EmailTemplate;
use App\Badges;
use App\Status;
use App\User;
use App\UserActivityLog;
use App\UserDeviceLog;
use App\Video;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Redirect;
use Validator;

class
UserActivationController extends Controller
{
    public function shareVideo($id){

        //HERE I HAVE CREATED OBJECT OF DEMO. YOU HAVE TO FETCH DATA FROM DATABASE MODEL
        // Eg: $shareData= User::whereId($id)->first();
        $shareData = new \stdClass();
        $shareData->id = 1;
        $shareData->title = "Share Title";
        $shareData->description = "Share Details";
        $shareData->image = "http://envoc.com/uploads/images/think-professional.jpg";
        $shareData->video_url = "https://www.youtube.com/";
        if($shareData){
            $data = array();
            $data['id'] = $shareData->id;
            $data['title'] = $shareData->title;
            $data['description'] = $shareData->description;
            $data['image'] = $shareData->image;
            $data['video_url'] = $shareData->video_url;
            return \View::make('share_file', ["data"=>$data]);
        }else{
            return \View::make('no_data_error');
        }


    }
}
